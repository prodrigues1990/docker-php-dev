This is a development container for PHP Phalcon Framework, can probably be used for other PHP based workloads.

# Usage

Build the image
```
docker build . -t php-dev
```

Generic container execution
```
docker run --name php-dev -d -v $(pwd):/app -p 80:80 php-dev <command>
```

## Start
Run the PHP dev web server (example for a Phalcon project)
```
docker run --name php-dev -d -v $(pwd):/app -p 80:80 php-dev php -S 0.0.0.0:80 -t public .htrouter.php
```


## Stop
```
docker php-dev stop
```


## Restart
```
docker php-dev restart
```


## Install composer dependencies
```
docker exec php-dev composer install
```

